# Inversify factory
Our game players can connect and receive round status data. We will build the another part of our game logic. Each player will start with a hand of cards. They will draw it randomly from a selected pets deck. We will start from preparing game components.

## Game 
**Game** is our main class that will be responsible for managing main game mechanics. Game object will be created when 2 players will join and destroyed when they will finish.

## Hand 
This class will represent player hand and his/her deck. It will be unique to each player and will keep track of cards they have in their have and play.

## Inversify
Instances of **Game** and **Hand** classes will be created by InversifyJS container. This will benefit us as application grows and application become more complicated. 

In PetWars game there will be multiple players. We will have to have an instance of Hand class for each player separately (each player have different cards in their hand). The simplest approach here is to define factory method in our IoC container. 

```
container.bind<interfaces.Factory<Hand>>("HandFactory").toFactory<Hand>((context: interfaces.Context) => {
  return () => {
      let hand: Hand = context.container.get<Hand>(Hand);
      return hand;
  };
});
```

When we inject HandFactory dependency to other class, we will receive method that will return a instance of a Hand. This way we can create as many separate instances of player hand as we need.

```
@inject("HandFactory") private handFactory!: () => Hand;
const hand: Hand = this.handFactory();
```

This approach can be extended by passing parameters to a factory method. In our case we will pass a deck name player uses.

```
container.bind<interfaces.Factory<Hand>>("HandFactory").toFactory<Hand>((context: interfaces.Context) => {
  return (fraction: string) => {
      let hand: Hand = context.container.get<Hand>(Hand);
      hand.selectDeck(fraction);
      return hand;
  };
});
```

## Emitting data
What left is to provide Hand of cards to our players. This is done by simple Web Socket emit.

```
start() {
  this.players.forEach((player) => {
    const cards = hand.getCards();
    player.emit('hand', cards); 
  });
}
```

Where players is a list of open Sockets to playing players. 