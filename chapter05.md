# Game logic
Our game server have all needed ingredients. We can assemble rest of our game logic.

## Board
This class will present all of pet wars actions. It will be designed in a way that it should work for two or more players. At the start of the game each player that joined will receive his/her empty board.

```
addPlayer(player: Player) {
  this.players.push(player);
  this.playerBoards.set(player, this.clearPlayerBoard());
}

```

When player(s) will play a card. Board will resolve game round and announce a winner (if there is one). This will be done simultaneously for all room players

```
resolveRound(announceWinner: (winner: Player) => void) {
  ...
}
```

## Round resolve

Sample round will be divided in following steps represented in Board resolveRound code

1. For each player for each board field count power of opponent pets
2. Pets that are 1 field from an opponent will fight and any received damage will be deducted from their life
3. Pets with life less or equal to zero are removed from game
4. Determine free spaces on the board
5. Move pets. If next board field in pets line is free, pet will move there. 
6. If players pet will move to the opposing end of the board. That player will win. Other player  will lose.
7. If nobody won in previous point selected from player hands cards are added to the board.
8. Board states are send to the players.

## Revenge
After game end with one of player being a winner. Player can start new game play with empty board and new hands by emitting "new" message.





