# API
For our game we will need a REST and WebSocket endpoints. We will use REST requests to get / post one time game data. For game play we will use two way communication to pass data between players. One of the ways to achieve that are Web Sockets

## REST API
REST is a transfer protocol that utilises current HTTP methods. For Java Script it is almost native with JSON as data format. We can use methods like

* GET - to get data from server
* POST - to add store data at server
* PUT - update data at server
* DELETE - remove data from server

For a full list please visit [https://www.w3schools.com/tags/ref_httpmethods.asp](https://www.w3schools.com/tags/ref_httpmethods.asp)

## Express web server

To expose REST API we need a web server. One of the most popular for Node.js is **Express** [https://expressjs.com/](https://expressjs.com/). First  of all we need to start express application.

```
const app: express.Application = express();
```

 We can now start our web server. To do so we need to provide it with a free port that server can use.

```
server.listen(process.env.PORT || 4000, function() {
  console.log("listening on *:4000");
});
```

## REST
We have our server running now. To check if everything is correct we will expose test endpoint. GET request is best way you can test, as it works in a simple browser/curl.

```
app.get("/api/battlefield", (req: any, res: any) => {
  res.send(JSON.stringify({test: 'test'}));
});
```

We cannot create 2 web servers on a same port. In current architecture our client and server will be running on separate web servers, we need to be prepared for that (more on that in Pet Wars client chapter 9 - Server connection). That is why all of our api endpoints will have */api* prefix. 

When we run our server, we can visit [http://localhos:4000/api/battlefield](http://localhos:4000/api/battlefield) to see if our test endpoint is working.

## Socket io
For our Web Sockets implementation we will use Socket.io library It is one of the mostly known and used for that kind of communication. For more detail [https://socket.io/](https://socket.io/).

We need to create and hook our WebSockets to already created Web Server.

```
const server = http.createServer(app);
let io = socketio(server);
```

So we create instance of http server based on our previously created app and pass it to our socketio library.

## Web Sockets
We created WebSocket server. We need it to react to client actions now. First we need to detect connections.

```
io.of('/ws').on("connection", (socket: any) => {
  console.log("Client connected");
  ...
});
```

When our client connects we will display log it. 

What about clients that will leave a game. We can use socket connection and listen for disconnection. For now we will just log when client disconnected.

```
io.of('/ws').on("connection", (socket: any) => {
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});
```

In similar way we will listen for messages form client.

```
io.of('/ws').on("connection", (socket: any) => {
  socket.on("message", (message: any) => {
    console.log(message)
  });
});
```

We are all set to prepare game logic now.

