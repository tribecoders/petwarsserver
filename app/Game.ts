import { injectable, inject, interfaces } from "inversify";
import Hand from "./Hand";
import { Socket, Namespace } from "socket.io";
import { Card } from "./Card";
import Board from "./Board";

export interface Play {
  cardIndex: number;
  position: number;
}

export interface Player {
  connection: Socket;
  hand: Hand;
  playedPosition?: number;
  playedCard?: Card;
}

@injectable()
export default class Game {
  @inject("HandFactory") private handFactory!: (fraction: string) => Hand;
  @inject(Board) private board!: Board;
  private room: Namespace | undefined;
  private players: Player[] = [];
  private countdown: NodeJS.Timer | undefined;



  addPlayer(connection: Socket) {
    // Initilise player hand
    const hand: Hand = this.handFactory('dogs');
    const player: Player = {connection, hand}
    this.board.addPlayer(player);

    connection.on('play', (play: Play) => {
      const playedCard = hand.playCard(play.cardIndex);
      player.playedPosition = play.position;
      player.playedCard = playedCard;
      if (!this.countdown) {
          this.room!.emit('message', 'countdown');
        // Wait 4 seconds for other players to commit cards
        this.countdown = setTimeout(() => {
          console.log('resolve cards');
          this.board.resolveRound(this.announceWinner.bind(this));
          // Deal cards for next round
          this.players.forEach(playerToUpdate => {
            const cards = playerToUpdate.hand.getCards();
            playerToUpdate.connection.emit('hand', cards);
          })
          this.countdown = undefined;
        }, 4000);
      }
    });

    connection.on('new', () => {
      if (!this.countdown) {
        this.room!.emit('message', 'countdown');
      }
      this.countdown = setTimeout(() => {
        this.board.reset();
        this.players.forEach(player => {
          player.hand.reset();
          const cards = player.hand.getCards();
          player.connection.emit('hand', cards);
          this.countdown = undefined;
        })
        this.room!.emit('message', 'new');
      }, 4000);
    });

    this.players.push(player);
  }

  announceWinner(winner: Player) {
    winner.connection.emit('message', 'winner');
    this.players.forEach(player => {
      if (player !== winner) {
        player.connection.emit('message', 'loser');
      }
    })
  }

  start(room: Namespace) {
    this.room = room;
    console.log('start');
    
    this.players.forEach((player) => {
      // return player cards
      const cards = player.hand.getCards();
      player.connection.emit('hand', cards); 
    });
  }

  pause() {
    console.log('pause');
  }

  stop() {
    console.log('stop');
  }
}