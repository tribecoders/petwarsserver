import { Card } from "./Card";
import Decks from "./Decks";
import { inject, injectable } from "inversify";

@injectable()
export default class Hand {
  @inject(Decks) private decks!: Decks
  private deck: Card[] = [];
  private poolSize = 8;
  private handSize = 4;
  private cards: number[] = [];
  private usedCards: number[] = [];

  constructor() {
    this.reset();
  }

  reset() {
    this.cards = [];
    for (let i = 0; i < this.poolSize; i++) {
      this.cards.push(i);
    }
    this.shuffleArray(this.cards);
  }

  selectDeck(fraction: string) {
    this.deck = this.decks.getFractionDeck(fraction);
  }

  private shuffleArray(array: any[]) {
    // Durstenfeld shuffle algorithm
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        let temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
  }

  getCards() {
    const handPositions = this.cards.slice(0, this.handSize);
    const handCards: Card[] = [];
    handPositions.forEach(position => {
      handCards.push(this.deck[position]);
    })
    return handCards;
  }

  playCard(cardIndex: number):Card {
    const playedCard = this.deck[this.cards[cardIndex]];
    this.usedCards.push(this.cards[cardIndex]);

    //When 4 cards are used. Used are shuffled and added back to the deck (so deck will be never deplited)
    if (this.cards.length <= this.handSize) {
      this.shuffleArray(this.usedCards);
      this.cards = this.cards.concat(this.usedCards);
      this.usedCards = [];
    }
    
    this.cards.splice(cardIndex, 1);
    return playedCard;
  }
}