import "reflect-metadata";
import { Container, interfaces } from 'inversify';
import Game from './Game';
import Hand from './Hand';
import Decks from './Decks';
import Board from "./Board";

export const container = new Container();
container.bind(Game).toSelf();
container.bind(Hand).toSelf();
container.bind(Board).toSelf();
container.bind<interfaces.Factory<Hand>>("HandFactory").toFactory<Hand>((context: interfaces.Context) => {
  return (fraction: string) => {
      let hand: Hand = context.container.get<Hand>(Hand);
      hand.selectDeck(fraction);
      return hand;
  };
});
container.bind(Decks).toSelf();