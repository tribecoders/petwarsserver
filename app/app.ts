import express from 'express';
import socketio, { Socket } from "socket.io";
import * as http from "http";
import { container } from './container';
import Game from './Game';

const app: express.Application = express();

const server = http.createServer(app);
const io = socketio(server).of('/ws');
const roomPlayers = new Map<string, number>();
const rooms = new Map<string, Game>()


app.get("/api/battlefield", (req: any, res: any) => {
  res.send(JSON.stringify({test: 'test'}));
});

io.on("connection", (socket: Socket) => {
  console.log("Client connected");
  socket.on('create', (room: string) => {
    console.log(`joining ${room}`)
    socket.join(room);
    if (roomPlayers.has(room)) {
      if (roomPlayers.get(room) === 1) {
        // Start game
        roomPlayers.set(room, 2)
        const game = rooms.get(room);
        if (game) {
          game.addPlayer(socket);
          game.start(io.to(room));
          io.to(room).emit('message', 'start');
        }
      } else {
        // Room is full
        socket.emit('message', 'full');
      }
    } else {
      // First player joined
      roomPlayers.set(room, 1);
      const game = container.get(Game);
      game.addPlayer(socket);
      rooms.set(room, game);
    }
  });

  socket.on("disconnecting", () => {
    console.log("Client disconnecting");
    const roomNames = Object.keys(socket.rooms);
    roomNames.forEach(room => {
      if (roomPlayers.has(room)) {
        // 1 player disconnected
        io.to(room).emit('message', 'stop');
        roomPlayers.set(room, 1);
        const game = rooms.get(room);
        if (game) {
          game.pause();
        }
      } else {
        // Both player disconnected
        roomPlayers.delete(room);
        rooms.delete(room)
      }
    });
  });
});

server.listen(process.env.PORT || 4000, function() {
  console.log("listening on *:4000");
});