import { Player } from "./Game";
import { Card } from "./Card";
import { injectable } from "inversify";

@injectable()
export default class Board {
  private numberOfRows = 3;
  private lineLength = 6;

  private players: Player[] = [];
  private playerBoards: Map<Player, (Card | undefined)[][]> = new Map()
  
  addPlayer(player: Player) {
    this.players.push(player);
    this.playerBoards.set(player, this.clearPlayerBoard());
  }

  reset() {
    this.playerBoards.forEach((board, player) => {
      const newBoard = this.clearPlayerBoard();
      this.playerBoards.set(player, newBoard);
      if (player.connection) {
        player.connection.emit('board', newBoard);
      }
    });
  }

  resolveRound(announceWinner: (winner: Player) => void) {
    // Count power of oponents for each player
    const oponentPower: Map<Player, number[][]> = new Map()
    this.players.forEach((player) => {
      // Count power of oponents at field for each player
      const oponentPowerGrid = this.countPowerGrid(player);
      oponentPower.set(player, oponentPowerGrid);
    });

    // Determine demage for the fights
    this.playerBoards.forEach((board, player) => {
      const oponentPowerGrid = oponentPower.get(player);
      if (oponentPowerGrid) {
        board.forEach((row, rowIndex) => {
          row.forEach((card, lineIndex) => {
            if (card) {
              const oponentAttack = oponentPowerGrid[rowIndex][this.lineLength - lineIndex - 2];
              const demage = card.defence <= oponentAttack ? oponentAttack - card.defence : 0;
              card.life -= demage;
              if (card.life <= 0) {
                // Remove defeted pet/card
                board[rowIndex][lineIndex] = undefined;
              }
            }
          });
        });
      }
    });

    // Determine availale (free) board spaces for each player
    const availableFields: Map<Player, boolean[][]> = new Map()
    this.players.forEach((player) => {
      // Free spaces do not have power. Power grid is recounted to remove values for killed oponents
      const availableFieldsGrid = this.countPowerGrid(player).map(row => row.map(
        field => field === 0 
      ));
      availableFields.set(player, availableFieldsGrid);
    });

    //Move
    this.playerBoards.forEach((board, player) => {
      board.forEach((row, rowIndex) => {
        let index = this.lineLength - 2;
        for(index; index >=0; index--) {
          const availableFieldsGrid = availableFields.get(player);
          if (availableFieldsGrid && availableFieldsGrid[rowIndex][this.lineLength - index - 2]) {
            // Check if other pets are blocking
            if (!row[index + 1]) {
              if (index + 1 === this.lineLength - 1 && row[index]) {
                announceWinner(player);
              }
              row[index + 1] = row[index];
              row[index] = undefined;
            }
          }
        }
      });
    });

    // Play cards selected by player
    this.playerBoards.forEach((board, player) => {
      if (player.playedCard && typeof player.playedPosition !== 'undefined') {
        board[player.playedPosition][0] = player.playedCard
      }
      player.playedCard = undefined;
    });

    //Send down results to players
    this.playerBoards.forEach((board, player) => {
      player.connection.emit('board', board);
      this.playerBoards.forEach((oponentBoard, oponent) => {
        if (oponent !== player) {
          player.connection.emit('enemy', oponentBoard)
        }
      });
    });
  };

  getPlayerBoard(player: Player) {
    return this.playerBoards.get(player);
  }

  private countPowerGrid(player: Player) : number[][] {
    let oponentPowerGrid: number[][] = Array.from(Array(this.numberOfRows), () => Array(this.lineLength).fill(0));

    this.playerBoards.forEach((oponentBoard, oponent) => {
      if(oponent !== player) {
        // Sum the power of each oponent at that field
        oponentPowerGrid = oponentPowerGrid.map((row, rowIndex) => {
          return row.map((power, lineIndex) => {
            let fieldPower = power;
            fieldPower += oponentBoard[rowIndex][lineIndex] ? oponentBoard[rowIndex][lineIndex]!.power : 0;
            fieldPower += oponentBoard[rowIndex][lineIndex - 1] ? oponentBoard[rowIndex][lineIndex - 1]!.power : 0;
            return fieldPower;
          })
        });
      }
    });
      
    return oponentPowerGrid
  }

  private clearPlayerBoard(): Card[][] {
    const board = Array.from(Array(this.numberOfRows), () => Array(this.lineLength).fill(undefined));
    return board;
  }
}