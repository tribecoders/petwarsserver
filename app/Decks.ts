
import * as cards from "./cards.json";
import { injectable } from "inversify";

@injectable()
export default class Decks {
  getFractionDeck (fraction: string) {
    let deck = cards[0].cards;
    for (let i = 0; i < cards.length; i++) {
      if (cards[i].name === fraction) {
        deck = cards[i].cards;
      }
    }
    return deck;
  }
}