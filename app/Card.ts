export interface Card {
  power: number;
  defence: number;
  life: number;
  image: string;
}

export interface CardDeck {
  name: string;
  cards: Card[]
} 
