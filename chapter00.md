# Pet Wars - Server

This is a second part of our Pet Wars game tutorial. The game idea is a duel between 2 players. We will use server to communicate and synchronise game state between the them.

You can find full sources of Pet Wars at [https://gitlab.com/tribecoders/petwars](https://gitlab.com/tribecoders/petwars). 

You can watch all of the steps in action at our Pet Wars demo page at [https://tribecoders.com/petwars](https://tribecoders.com/petwars)

In our project will be using:

## Node.js
Node.js is a Java Script library that adopt Java Script language to work on Chrome V8 engine as a server technology. This way you can code both client and server in the same programing language. More at [https://nodejs.org/](https://nodejs.org/).

## NPM
If you are not familiar with this tool. This is a package manager for JavaScript projects. It will helps us to organise code dependencies in our ever growing code base. See [https://www.npmjs.com/](https://www.npmjs.com/) for more details.

## WebPack
This is a module bundler for modern JavaScript applications. From managing scripts and images, to compiling SCSS, TypeScript. See documentation at [https://webpack.js.org/](https://webpack.js.org/)

## TypeScript
This is optional typing library for JavaScript. Details about TypeScript can be found at [https://www.typescriptlang.org/](https://www.typescriptlang.org/). It is not mandatory for any of JavaScript projects. I strongly encourage you to use it in medium to big projects. Especially if you expect that there will be more collaborators. It gives out of the box JavaScript code a structure and keeps it easy to ready. It maybe looks a little formal at first. But after a while you will see benefits of using it. Especially for games where from a really small project you will gradually build bigger and bigger code base.