FROM node:10

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

COPY ./app/public ./public

RUN npm install

USER node

COPY --chown=node:node . .

EXPOSE 3001

CMD npm run prod