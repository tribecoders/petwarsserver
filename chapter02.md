# Web Socket rooms

Our players will join in pairs connected with common string. After connecting they will communicate in pairs enabling gameplay. One solution that come with Web Sockets is **rooms**

## Joining rooms
We already catch player connection and sample message. We can use this methods to assign him/her a room.

When will launch Pet Wars UI. He/She will select a room (simple string) to create or join. From server point of view creation and joining is the same operation. We just need to simply listen for client emitting **create** message with room id value

```
socket.on('create', (room: string) => {
  ...
}
```

We will join this room with provided room name

```
socket.join(room);
```

## Leaving rooms
What about players that will close the browser or lock their phones. They will not be able to play and will need to leave our room.

We need to listen for player *disconnecting* socket event to detect it.

```
socket.on("disconnecting", () => {
  ...
}
```

Now we can get all of player rooms and disconnect.

```
const rooms = Object.keys(socket.rooms);
```

## Game state tracking
With above events in place we can monitor number of players in each room. We will create a simple map with room name and number of players that already joined it

```
const roomPlayers = new Map<string, number>();
```

Now we will track player count. Add 1 when player join to a room, deduct 1 when player disconnects and emit following status events

* start - when 2 players joined the same room
* stop - when 1 or both players disconnected
* full - when player joined a room that is already full

Connect:
```
if (roomPlayers.has(room)) {
  if (roomPlayers.get(room) === 1) {
    // Start game
    roomPlayers.set(room, 2)
    io.to(room).emit('message', 'start');
  } else {
    // Room is full
    socket.emit('message', 'full');
  }
} else {
  // First player joined
  roomPlayers.set(room, 1);
}
```

Disconnect:
```
rooms.forEach(room => {
  if (roomPlayers.has(room)) {
    // 1 player disconnected
    roomPlayers.set(room, 1);
    io.to(room).emit('message', 'stop');
  } else {
    // Both player disconnected
    roomPlayers.delete(room);
  }
});
```

