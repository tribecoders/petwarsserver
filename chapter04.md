# Two way communication
In previous chapter we could see how players shuffle and receive their starting hands. We will introduce more interaction now. Our first goal is to simulate game round. Player will pick their card from their hand. This will initiate round countdown clock,  giving other play last chance to decide what card to play. After cards will be resolved new card will be dealt to a player and new round will start.

## Receiving an event
When a player will choose a car in our game client, pet wars server will receive a play event. Server needs to listen for such an event and process it. To do that we will attach "play" event listener in our Game class.

```
connection.on('play', (play: Play) => {
  ...
}
```

Our play event will indicate which card was played and on what field. Selected card will be based on a card position in player hand. This way player cannot change played card values. Below you can find "play" event data structure.

```
export interface Play {
  cardIndex: number;
  position: number;
}
```

Card index is a index of an card in the hand and position is a row card should be played. 

## Emitting a message
After receiving of the event we will start round end counter. We need to notify all players at the table about this fact.

```
if (!this.countdown) {
  this.room.emit('message', 'countdown');
}
```

We need to verify if our countdown was not already stated (so we do not initiate it twice). Afterwards we emit a global room message to let all players know that round is about to finish.

## Round end
On a round end we will advance all card, resolve fights and place new cards on the board. This will done in the next chapter. We will concentrate on hand management now. Player played a card and now we need to deal another one back.  

```
Hand.ts
playCard(cardIndex: number):Card {
    this.cards.splice(cardIndex, 1);
    this.usedCards.push(this.cards[cardIndex]);
}
```

When card is played we will remove this card from current deck of cards and place it in used cards array. This way if array is long enough first 4 elements of array will always be our full hand.

```
Game.ts
this.countdown = setTimeout(() => {
  const cards = player.hand.getCards();
  this.room!.emit('hand', cards);
  this.countdown = undefined;
}, 4000);
```

When the timeout will fire. We will get these cards and send down to our player. But what about the situation when our deck become to short.

```
Hand.ts
if (this.cards.length <= this.handSize) {
  this.shuffleArray(this.usedCards);
  this.cards = this.cards.concat(this.usedCards);
  this.usedCards = [];
}
````

Now when we play a card and cause a deck to become shorter than a hand cards. We will shuffle already used cards and attach to the end of the deck. 






